<div>
    <h1>Maksu- ja toimitusehdot</h1>
        
    <h3>Toimitus</h3>
        <p>Hyvä asiakas, saat vahvistuksen tilauksestasi sähköpostiisi heti tilauksen jälkeen.
         <p>   Jos tuotetta on varastossa lähetämme tilauksesi seuraavana arkipäivänä. Kun tilaamme tuotetta lisää varastoomme lähetys aika saattaa olla 5-8 arkipäivää. </p>
        </p>
        <p>Toimitustavat ja toimituskulut (€):</p>
        <p>Nouto (sovitulta paikalta). Nouto on maksuton.</p>
        <p>Toimitus lähimpään postin toimituspaikkaan 5-12€ per lähetys. Toimituksen hinta riippuu paketin koosta ja painosta.</p>
        <p>Postin kotiinkuljetus 20,00 € per lähetys.</p>
        <p>Nouto Postin pakettiautomaatista. Hinta on 9.90€-17.90€ per lähetys.</p>
        <p>Muun kuljetuspalvelun kotiinkuljetus 10,00 € per lähetys.</p>
        <p>Yli 70 € ostokset veloituksetta lähimpään postin toimituspaikkaan.</p>
        <p>Yli 120 € ostokset veloituksetta kotiovellesi.</p>
    <h3>Nouto postista</h3>
        <p>Toimitetaan lähimpään postiin, 
            josta voit paketin noutaa saatuasi saapumisilmoituksen. 
            Saat ohjeet noutamiseen tekstiviestillä paketin saapuessa. </p>
        <p>Henkilöllisyys pitää todistaa noudon yhteydessä.</p>
    <h3>pakettiautomaatista noutaminen</h3>
        <p>Nouda tilauksesi valitsemastasi pakettiautomaatista.  
            Saat ohjeet noutamiseen tekstiviestillä paketin saapuessa.
             Muista tarkistaa pakettiautomaatin sijainti.</p>
    <h3>Kotiinkuljetus</h3>
        <p>Paketin kotiinkuljetuksen takia kuljettaja ottaa sinuun yhteyttä, jotta kuljetuksen ajankohta voidaan sopia.
        Kotiinkuljetukset arkipäivisin klo 9-16</p>
        <p>
        Valitettavasti emme aina voi luvata, että kotiinkuljetus onnistuu. Pakettien kotiinkuljetus palvelut eivät välttämättä kuljeta paketteja kaikkialle Suomeen. Jos kuljetus ei onnistu paketti toimitetaan lähimpään postin jakelupisteeseen. 
        </p>
        <p>
        HUOM Kun valitset tuotteelle kotiinkuljetuksen niin palvelu ei koske palautuksia.</p>
       <p> Mahdollisessa palautus tilanteessa asiakkaan tulee itse toimittaa postin kautta palautettava paketti. </p>
    <h3>Nouto</h3>
        <p>Voit halutessasi noutaa tilauksesi varastoltamme tai sovitusta nouto paikasta. 
            Saat ilmoituksen kun voit noutaa tilauksesi. Nouto on maksuton.</p>
        <p>Henkilöllisyys pitää todistaa noudon yhteydessä.</p>
    <h3>Toimitusaika</h3>
        <p>tilauksesi lähetetään 1-4 arkipäivän kuluessa. 
            Jos tuote on loppunut varastosta, otamme sinuun yhteyttä mahdollisimman nopeasti.</p>
        <p>Emme ole vastuussa Postin tai muiden kuljetus firmojen viiveistä tai mahdollisista muutoksista ja niiden seurauksista.</p>
    <h3>Ostoskori</h3>
        <p>Verkkokauppamme toimii ostoskori-periaatteella. Ostoskori näkyy aina selaimessa oikeassa ylälaidassa.</p>
        <p>Ostoskorissa voit:<p>
        <p>Poistaa tuotteita<p>
        <p>Vaihtaa ostoskorissa olevan tuotteen määrää<p>
        <p>Tyhjentää ostoskorin vain yhtä nappia painamalla.<p>
        <p>Klikkaamalla ostoskorissa olevaa tuotetta pääset selaamaan tuotteen tietoja</p>
        <p>Muista valita haluamasi maksu- ja toimitustapa tilauksen yhteydessä</p>
    <h3>Vaihto- ,palautus- ja peruutusoikeus</h3>
        <p>Tästä verkkokaupasta ostamasi tuotteet ovat elintarvike-tuotteita, jolloin tuotteilla ei ole palautusoikeutta.
            Jos tuotetta ei ole noudettu, tilaus on silti voimassa. </p>
            <p>Veloitamme uudelleen lähetyksestä 20€.</p>
        <p>Tietyissä tilanteissa kun tuote tai tilaus on viallinen, niin palautus tai vaihtaminen on maksutonta.</p>
        <p>Mikäli paketti tai sen sisältö on kuljetuksessa vioittunut, tee siitä ilmoitus postille vastaanottaessasi pakettia, jonka jälkeen ota yhteys asiakaspalveluumme!</p>
    <h3>Tilaaminen</h3>
        <p>Verkkokauppaamme voit halutessasi rekisteröityä/kirjautua, mutta tilaaminen onnistuu myös ilman kirjautumista.</p>
            <p>Kun olet lisännyt ostoskoriin kaiken haluamasi paina ostoskori painiketta selaimessa oikealla ylhäällä.</p>
            Varmista, että haluamasi tuotteet ovat ostoskorissa. Paina sitten tilaa painiketta ja täytä tilauslomakkeeseen tarvittavat tiedot (toimitustapa, maksutapa, omat tiedot).
            Tämän jälkeen voit maksaa ostoksesi. </p>
            <p>HUOM hinnat ovat euroissa(€).</p>
        <p>Tuotteiden saatavuustiedot löydät tuotteiden omilta sivuilta. 
            Mikäli tilattua tuotetta ei ole varastossa otamme asiakkaaseen yhteyttä mahdollisimman pian ja korjaamme tilauksen asiakkaan haluamalla tavalla.</p>
        <p>Kaikki tuotteitamme on saatavilla vain rajattu määrä.</p>
    <h3>Maksutavat</h3>
        <p>Meillä maksutavoiksi käyvät:</p>
        <p>Klarna</p>
        <p>Verkkopankki</p>
        <p>Lasku</p>
        <p>Erämaksu</p>
        <p>Luottokortti</p>
        <p>Mobilepay</p>
        <p>Kaikki tavat ovat yhtä turvallisia.</p>
    <h3>Asiakastietojen säilyttäminen ja luovutus</h3>
        <p>Käsittelemme kaikki asiakastiedot täysin luottamuksellisesti ja sitoudumme olemaan luovuttamatta asiakastietoja kolmannelle osapuolelle.</p>
    <h3>Kysyttävää?</h3>
        <p>Ota yhteyttä:</p>
        <p>Herkkuhetki OY</p>
        <p>Joutsenkatu 12</p>
        <p>34234 Joutsenkoski</p>
        <p>Puh. 03012345678</p>
        <p>Email: herkkuhetki@verkkokauppa.fi</p>
</div>